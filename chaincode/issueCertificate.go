package chaincode

import (
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	jsoniter "github.com/json-iterator/go"
	"log"
)

func (s *SmartContract) IssueCertificate(ctx contractapi.TransactionContextInterface, payload string) error {

	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	// validate issuerOrg
	iorg, err := getSubmittingClientOrgMsp(ctx)
	if err != nil {
		log.Printf("unable to get Submitting Client Identity %v\n",err)
		return err
	}


	// open data model is passed normally

	var data Payload
	err = json.Unmarshal([]byte(payload), &payload)
	if err != nil {
		log.Println("unable to unmarshal payload")
		log.Println(err.Error())
		return err
	}
	data.Issuer = iorg
	// check if already exist id
	exists, err := s.CertificateExists(ctx, data.Certificate.Id)
	if err != nil {
		log.Println("unable to check certificate if exists")
		log.Println(err.Error())
		return err
	}
	if exists {
		log.Printf("the certificate %s already exists\n", data.Certificate.Id)
		return fmt.Errorf("the certificate %s already exists", data.Certificate.Id)
	}

	stub := ctx.GetStub()


	// raise the information as event
	err = stub.SetEvent("CertificateIssued", []byte(payload))
	if err != nil {
		log.Printf("unable to set event CertificateIssued, %v\n",err)
		return err
	}
	// store open data
	err = stub.PutState(data.Certificate.Id, []byte(payload))
	if err != nil {
		log.Printf("unable to set state, %v\n",err)
		log.Println(err.Error())
		return err
	}

	// confidential data is passed as private data.

	// Get new certificate from transient map
	transientMap, err := stub.GetTransient()
	if err != nil {
		log.Printf("unable to Get Transient, %v\n",err)
		log.Println(err.Error())
		return fmt.Errorf("error getting transient: %v", err)
	}

	// Asset properties are private, therefore they get passed in transient field, instead of func args
	transientCertJSON, ok := transientMap["cert_data"]
	if !ok {
		//log error to stdout
		log.Println("certificate not found in the transient map input")
		return fmt.Errorf("certificate not found in the transient map input")
	}

	var cert Certificate

	err = json.Unmarshal(transientCertJSON, &cert)
	if err != nil {
		log.Printf("failed to unmarshal JSON: %v\n", err)
		return fmt.Errorf("failed to unmarshal JSON: %v", err)
	}

	collection := getCollectionName()

	// Check if asset already exists
	certBytes, err := ctx.GetStub().GetPrivateData(collection, cert.Id)
	if err != nil {
		log.Printf("failed to get private data: %v\n", err)
		return fmt.Errorf("failed to get private data: %v", err)
	} else if certBytes != nil {
		log.Printf("Certificate already exists: %v\n", cert.Id)
		return fmt.Errorf("this certificate already exists: " + cert.Id)
	}

	// Save cert to private data collection
	// Typical logger, logs to stdout/file in the fabric managed docker container, running this chaincode
	// Look for container name like dev-peer0.org1.example.com-{chaincodename_version}-xyz
	log.Printf("CreateAsset Put: collection %v, ID %v, owner %v\n", collection, cert.Id, iorg)

	err = ctx.GetStub().PutPrivateData(collection, cert.Id, transientCertJSON)
	if err != nil {
		log.Printf("failed to put certificate into private data collecton: %v", err)
		log.Println(err.Error())
		return fmt.Errorf("failed to put certificate into private data collecton: %v", err)
	}
	return nil
}
