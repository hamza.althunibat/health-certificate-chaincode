package chaincode

import (
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/json-iterator/go"
)

type ServerConfig struct {
	CCID    string
	Address string
}

// SmartContract provides functions for managing an Certificate
type SmartContract struct {
	contractapi.Contract
}


func getOpenCert(ctx contractapi.TransactionContextInterface, id string, json jsoniter.API) (*Payload, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if assetJSON == nil {
		return nil, fmt.Errorf("the certificate %s does not exist", id)
	}

	var payload Payload
	err = json.Unmarshal(assetJSON, &payload)
	if err != nil {
		return nil, err
	}

	return &payload, nil
}

func contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}
// CertificateExists returns true when asset with given ID exists in world state
func (s *SmartContract) CertificateExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return assetJSON != nil, nil
}

// getCollectionName is an internal helper function to get collection of submitting client identity.
func getCollectionName() string {
	return "health_certificate_collection"
}

func getSubmittingClientOrgMsp(ctx contractapi.TransactionContextInterface) (string, error) {
	return ctx.GetClientIdentity().GetMSPID()
}
