package chaincode

import (
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	jsoniter "github.com/json-iterator/go"
)

// ReadOpenCertificate returns the asset stored in the world state with given id.
func (s *SmartContract) ReadOpenCertificate(ctx contractapi.TransactionContextInterface, id string) (*Payload, error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return getOpenCert(ctx, id, json)
}

