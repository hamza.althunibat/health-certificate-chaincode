module gitlab.com/hamza.althunibat/health-certificate-chaincode

go 1.16

require (
	github.com/hyperledger/fabric-chaincode-go v0.0.0-20210603161043-af0e3898842a
	github.com/hyperledger/fabric-contract-api-go v1.1.1
	github.com/hyperledger/fabric-protos-go v0.0.0-20210528200356-82833ecdac31 // indirect
	github.com/json-iterator/go v1.1.11
)
